import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'translate';
  languages = ["Tiếng Nga","Tiếng Việt","Tiếng Anh"]
}
